import yaml

def read_yaml(file_path):
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

# Function to write data to a YAML file
def write_yaml(data, file_path):
    with open(file_path, 'w') as file:
        yaml.safe_dump(data, file)
    
data = read_yaml('./pipelineRun/pipelineRun.yaml')

data['spec']['params'][0]['value'] = "valuechanged"

# Write the modified data back to a new YAML file
write_yaml(data, 'pipelineRun.yaml')

print(data)